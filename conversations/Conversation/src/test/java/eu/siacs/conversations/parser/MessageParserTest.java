package eu.siacs.conversations.parser;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import eu.siacs.conversations.entities.Account;
import eu.siacs.conversations.services.XmppConnectionService;
import eu.siacs.conversations.xmpp.stanzas.MessagePacket;

import static org.junit.Assert.*;

/**
 * Created by Laptop_Ezra on 23-1-2017.
 */
public class MessageParserTest {

    @Mock
    Account accMock;

    @Mock
    MessagePacket expectedResult;

    @Mock
    XmppConnectionService xmppService;


    @Before
    public void Setup(){
        expectedResult = setupExpected();
        xmppService = Mockito.mock(XmppConnectionService.class);
        accMock = Mockito.mock(Account.class);
    }

    @Test
    public void OnMessageReceivedTest(){

        MessageParser parser = new MessageParser(xmppService);

        parser.onMessagePacketReceived(accMock, expectedResult);


    }

    private static MessagePacket setupExpected(){
        MessagePacket expected = new MessagePacket();
        expected.setType(MessagePacket.TYPE_CHAT);
        expected.addChild("markable", "urn:xmpp:chat-markers:0");
        return expected;
    }
}