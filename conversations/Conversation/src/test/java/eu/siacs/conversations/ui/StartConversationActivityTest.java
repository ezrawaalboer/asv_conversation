package eu.siacs.conversations.ui;

import android.content.Intent;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Laptop_Ezra on 24-1-2017.
 */
public class StartConversationActivityTest {


    @Test
    public void testHandleIntentWithEmptyViewAction() throws Exception {
        StartConversationActivity test = Mockito.mock(StartConversationActivity.class);
        Intent testIntent = Mockito.mock(Intent.class);

        when(testIntent.getAction()).thenReturn(Intent.ACTION_VIEW);
        when(testIntent.getData()).thenReturn(null);

        boolean value = test.handleIntent(testIntent);

        assertFalse(value);
    }



}