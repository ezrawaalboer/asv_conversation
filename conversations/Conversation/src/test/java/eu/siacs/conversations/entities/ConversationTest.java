package eu.siacs.conversations.entities;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;



/**
 * Created by Laptop_Ezra on 10-1-2017.
 */
public class ConversationTest {

    @Test
    public void testGreaterThen() throws Exception {
        assertThat(Conversation.greaterThen(6), is(true));

    }

}