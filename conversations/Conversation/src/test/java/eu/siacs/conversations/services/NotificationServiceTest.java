package eu.siacs.conversations.services;

import android.content.SharedPreferences;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import eu.siacs.conversations.entities.Account;
import eu.siacs.conversations.entities.Conversation;
import eu.siacs.conversations.entities.Message;
import eu.siacs.conversations.xmpp.jid.Jid;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Laptop_Ezra on 23-1-2017.
 */
public class NotificationServiceTest {

    @Mock
    Message messageMock;

    @Mock
    Conversation convMock;

    @Mock
    Account accMock;

    @Mock
    Jid jIdmock;

    @Mock
    XmppConnectionService xmppService;

    @Mock
    SharedPreferences preferences;


    @Test
    public void push() throws Exception {

        messageMock = Mockito.mock(Message.class);
        convMock = Mockito.mock(Conversation.class);
        accMock = Mockito.mock(Account.class);
        xmppService = Mockito.mock(XmppConnectionService.class);
        preferences = Mockito.mock(SharedPreferences.class);
//
        when(xmppService.getPreferences()).thenReturn(preferences);

        when(convMock.getAccount()).thenReturn(accMock);

        when(messageMock.getConversation()).thenReturn(convMock);

        NotificationService notService = new NotificationService(xmppService);

        assertFalse(notService.notify(messageMock));

    }

}