package eu.siacs.conversations.generator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.net.MalformedURLException;
import java.net.URL;

import eu.siacs.conversations.entities.Account;
import eu.siacs.conversations.entities.Conversation;
import eu.siacs.conversations.entities.Message;
import eu.siacs.conversations.services.XmppConnectionService;
import eu.siacs.conversations.xmpp.jid.Jid;
import eu.siacs.conversations.xmpp.stanzas.MessagePacket;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.Is.isA;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

/**
 * Created by Laptop_Ezra on 23-1-2017.
 */
public class MessageGeneratorTest {

    @Mock
    Message messageMock;

    @Mock
    Conversation convMock;

    @Mock
    Account accMock;

    @Mock
    Message.FileParams fileParamsMock;

    @Mock
    XmppConnectionService xmppService;

    @Before
    public void prepareMessage(){
        messageMock = Mockito.mock(Message.class);
        convMock = Mockito.mock(Conversation.class);
        accMock = Mockito.mock(Account.class);
        xmppService = Mockito.mock(XmppConnectionService.class);
        fileParamsMock = Mockito.mock(Message.FileParams.class);

        try {
            fileParamsMock.url = new URL("http://nu.nl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        when(messageMock.getFileParams()).thenReturn(fileParamsMock);
        when(convMock.getAccount()).thenReturn(accMock);
        when(messageMock.getConversation()).thenReturn(convMock);
    }

    @Test
    public void testGenerateChatFromMessageWithHasFile() throws Exception {
        MessagePacket expectedResult = setupExpected();
        expectedResult.setBody("http://nu.nl");
        expectedResult.addChild("x", "jabber:x:oob").addChild("url").setContent("http://nu.nl");

        Message m = messageMock;
        when(m.hasFileOnRemoteHost()).thenReturn(true);
        when(m.getBody()).thenReturn("This is a body");

        MessageGenerator generator = new MessageGenerator(xmppService);
        MessagePacket packet = generator.generateChat(m);

        assertEquals(expectedResult.toString(), packet.toString());

    }

    @Test
    public void testGenerateChatFromMessageWithNoFile() throws Exception {
        MessagePacket expectedResult = setupExpected();
        expectedResult.setBody("This is a body");

        Message m = messageMock;
        when(m.hasFileOnRemoteHost()).thenReturn(false);
        when(m.getBody()).thenReturn("This is a body");

        MessageGenerator generator = new MessageGenerator(xmppService);
        MessagePacket packet = generator.generateChat(m);

        assertEquals(expectedResult.toString(), packet.toString());

    }

    @Test
    public void generatePgpChat() throws Exception {
        MessageGenerator mesGen = new MessageGenerator(xmppService);

        MessagePacket packet = mesGen.generatePgpChat(messageMock);

        MessagePacket expectedResult = setupExpected();
        expectedResult.setBody("I sent you a PGP encrypted message but your client doesn’t seem to support that.");

        assertEquals(expectedResult.toString(), packet.toString());

    }

    private static MessagePacket setupExpected(){
        MessagePacket expected = new MessagePacket();
        expected.setType(MessagePacket.TYPE_CHAT);
        expected.addChild("markable", "urn:xmpp:chat-markers:0");
        return expected;
    }

}