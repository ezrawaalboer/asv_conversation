package eu.siacs.conversations.utils;

import android.content.Context;
import android.util.Log;
import android.util.Pair;

import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.exceptions.misusing.MockitoConfigurationException;

import eu.siacs.conversations.entities.Message;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by Laptop_Ezra on 23-1-2017.
 */
public class UIHelperTest {

    @Mock
    Context context;

    @Mock
    Message message;

    @Test
    public void getMessagePreview() throws Exception {

        context = Mockito.mock(Context.class);
        message = Mockito.mock(Message.class);

        when(message.getBody()).thenReturn("test message");

        Pair<String,Boolean> test = UIHelper.getMessagePreview(context, message);

        Log.e("test", test.toString());

    }

}